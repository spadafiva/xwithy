## XwithY

### Getting Started
This app uses cocoapods for the Alamofire pod.  If you don't have cocoapods installed, please follow these directions:

[Cocopods Install](https://guides.cocoapods.org/using/getting-started.html)

### Concept
This app helps you make a decision about finding something fun to do with a friend.


### To Do
1. Add Testing 
2. Add Error Messages when the guards fail in `ViewController.showViews()`


### Upcoming Possible Enhancements
1. Add MFMailComposer to reach out to the email address in the user object to invite them to listen to the album
