//
//  ViewController.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //  MARK: - Identifier Constants
    //  MARK: - Interface Builder Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var withLabel: UILabel!
    
    @IBOutlet weak var personLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    
    @IBOutlet weak var topViewCenterContstraint: NSLayoutConstraint!
    @IBOutlet weak var withLabelConstraint:NSLayoutConstraint!
    @IBOutlet weak var bottomViewConstraint:NSLayoutConstraint!
    
    //  MARK: - Properties
    var users: [User]?
    var albums: [Album]?
    
    var currentMatchedUser: User? {
        didSet {
            personLabel.text = currentMatchedUser?.name
        }
    }
    
    var currentMatchedAlbum: Album? {
        didSet{
            albumLabel.text = currentMatchedAlbum?.musicTitle
        }
    }
    
    var wOffset: CGFloat {
        return self.view.bounds.width
    }
    
    var hOffset: CGFloat {
        return self.view.bounds.height
    }
    
    lazy var api = Api()


    //  MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideViews()
        
        api.getUsers { _, users in
            self.users = users
        }
        
        api.getAlbums { _, albums in
            self.albums = albums
        }
        
    }

    //  MARK: - IBActions
    @IBAction func whatToDoPressed(_ sender: UIButton) {
        hideViews()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [weak self] in
            self?.showViews()
        }
    }

    //  MARK: - Helper Functions
    func hideViews() {
        topViewCenterContstraint.constant = -wOffset
        bottomViewConstraint.constant = wOffset
        withLabelConstraint.constant = hOffset
        
        view.setNeedsLayout()
    }
    
    func showViews() {
        
        guard let users = users, let albums = albums else { return }
        guard let pairing = ActivityMatcher.randomPair(users, and: albums) else { return }
        
        currentMatchedUser = pairing.0
        currentMatchedAlbum = pairing.1
        
        
        for constraint in [topViewCenterContstraint, withLabelConstraint, bottomViewConstraint] {
            constraint?.constant = 0
        }
        
        UIView.animate(withDuration: 0.6) {
            self.view.layoutIfNeeded()
        }
    }

}

