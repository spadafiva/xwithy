//
//  Album.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import Foundation

struct Album {
    let id: Int
    let musicTitle: String
    
    enum JSONKey: String {
        case id
        case musicTitle = "title"
    }
}

extension Album: JSONRepresentable {
    init?(json: [String: Any]?) {
        guard let json = json else { return nil }
        guard let id = json[JSONKey.id.rawValue] as? Int,
              let musicTitle = json[JSONKey.musicTitle.rawValue] as? String else {
                return nil
        }
        
        self.init(id: id, musicTitle: musicTitle)
    }
}
