//
//  ActivityMatcher.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import Foundation

class ActivityMatcher {
    static func randomPair<A, B>(_ a: [A], and b: [B]) -> (A, B)? {
        guard a.count > 0, b.count > 0 else { return nil }
        return (a.randomItem(), b.randomItem())
    }
}

fileprivate
extension Array {
    func randomItem() -> Array.Iterator.Element {
        return self[Int(arc4random_uniform(UInt32(self.count)))]
    }
}
