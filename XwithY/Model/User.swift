//
//  User.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import Foundation

struct User {
    let id: Int
    let name: String
    let email: String
    
    enum JSONKey: String {
        case id
        case name
        case email
    }
}

extension User: JSONRepresentable {
    init?(json: [String: Any]?) {
        guard let json = json else { return nil }
        guard let id = json[JSONKey.id.rawValue] as? Int,
            let name = json[JSONKey.name.rawValue] as? String,
            let email = json[JSONKey.email.rawValue] as? String else {
                return nil
        }
        
        self.init(id: id, name: name, email: email)
    }
}

/*
 
 This pattern made more sense in a different app that I built where I was 
 returning a Result<T> enum, so that if it couldn't be cast as `T` that it would
 return and error instead in the else of the if -let.  Since this doesn't 
 have that, I've just used the enum to avoid some stringly based errors, but
 removed the two dictionary extensions. 
 
fileprivate
extension Dictionary where Key == String {
    func value<T>(forKey key: User.JSONKey, ofType: T.Type) -> T? {
        if let foundKey = self[key.rawValue] as? T {
            return foundKey
        }
        return nil
    }
}
*/
