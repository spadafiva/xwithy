//
//  Api.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import Alamofire

class Api {
    private let root = "https://jsonplaceholder.typicode.com"

    func getUsers(completion: @escaping (Error?, [User]?) -> Void) {
        Alamofire
            .request(root + "/users",
                     method: .get,
                     parameters: nil,
                     encoding: JSONEncoding.default,
                     headers: nil)
            .validate()
            .responseJSON { result in
                
                let userResult = validate(result, asArrayType: [User].self)
                completion(userResult.error, userResult.value)

        }
    }
    
    func getAlbums(completion: @escaping (Error?, [Album]?) -> Void) {
        Alamofire
            .request(root + "/albums",
                     method: .get,
                     parameters: nil,
                     encoding: JSONEncoding.default,
                     headers: nil)
            .validate()
            .responseJSON { result in
                
                let userResult = validate(result, asArrayType: [Album].self)
                completion(userResult.error, userResult.value)
                
        }

    }
    

    
}

fileprivate
func validate<T>(_ result: DataResponse<Any>, asArrayType type: T.Type) -> (error: Error?, value: [T.Iterator.Element]?) where T: Collection, T.Iterator.Element: JSONRepresentable {
    
    guard let jsonArray = result.value as? [[String: Any]] else { return (result.error, nil) }
    let values = jsonArray.flatMap({ T.Iterator.Element(json: $0) })
    
    return (result.error, values)
    
}

protocol JSONRepresentable {
    init?(json: [String: Any]?)
}

