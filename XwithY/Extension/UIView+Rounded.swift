//
//  UIView+Rounded.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import UIKit

extension UIView {
    func makeRounded(radius: CGFloat = 5) {
        
        /*
         https://developer.apple.com/documentation/uikit/uiview/1622572-translatesautoresizingmaskintoco
         
         By default, the property is set to true for any view you 
         programmatically create. If you add views in Interface Builder, 
         the system automatically sets this property to false.
         
         In other cases, I have laid items and added views 100% programatically
         while also using autolayout in that code.  It looks like I don't need 
         to use this for storyboard code though.
         
        translatesAutoresizingMaskIntoConstraints = false

         */
        
        layer.cornerRadius = radius
        layer.masksToBounds = false
    }
}
