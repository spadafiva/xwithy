//
//  RoundedView.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        makeRounded()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        makeRounded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        makeRounded()
    }
}
