//
//  AppDelegate.swift
//  XwithY
//
//  Created by Joseph Spadafora on 8/8/17.
//  Copyright © 2017 Swift Joe. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

